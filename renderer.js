const { shell, ipcRenderer } = require('electron');
const buttercup = require("buttercup");
const check = require('./check');

const hero = document.getElementById('hero');
const selectArchiveButton = document.getElementById('select-archive');
const passwordInput = document.getElementById('password');
const passwordDiv = document.getElementById('password_div');
const noPwnedPasswordsSpan = document.getElementById('no_pwned_passwords_span');
const pwnedPasswordsSection = document.getElementById('pwned_passwords_section');
const pwnedPasswordsTable = document.getElementById('pwned_passwords_table');
const pwnedPasswordsTableBody = document.getElementById('pwned_passwords_tbody');

selectArchiveButton.addEventListener('click', (event) => {
  ipcRenderer.send('open-file-dialog');
});

ipcRenderer.on('selected-archive', (event, paths) => {
  if (paths.length !== 1)
    alert('Too many files selected. Will only use the first one.');
  const datasource = new buttercup.Datasources.FileDatasource(paths[0]);
  selectArchiveButton.style.display = 'none';
  passwordDiv.style.display = 'block';
  passwordInput.addEventListener('keyup', event => {
    event.preventDefault();
    if (event.keyCode === 13) { // Enter
      passwordInput.disabled = true;
      const password = passwordInput.value;
      check(datasource, password)
        .then(async pwned => {
          passwordDiv.style.display = 'none';
          console.log(pwned);
          if (pwned.length) {
            hero.style.display = 'none';
            pwnedPasswordsSection.style.display = 'block';
            const credentials = buttercup.Credentials.fromPassword(password);
            datasource
              .load(credentials)
              .then(buttercup.Archive.createFromHistory)
              .then(archive => {
                pwned.forEach(pwnedEntryID => {
                  const entry = archive.findEntryByID(pwnedEntryID);
                  const data = entry.getProperty();
                  const tr = document.createElement('tr');
                  const titleTd = document.createElement('td');
                  const usernameTd = document.createElement('td');
                  const passwordTd = document.createElement('td');
                  if (data.URL) {
                    const titleA = document.createElement('a');
                    titleA.textContent = data.title;
                    titleA.addEventListener('click', event => {
                      shell.openExternal(data.URL);
                    });
                    tr.appendChild(titleA);
                  } else {
                    titleTd.textContent = data.title;
                    tr.appendChild(titleTd);
                  }
                  usernameTd.textContent = data.username;
                  passwordTd.textContent = data.password;
                  tr.appendChild(usernameTd);
                  tr.appendChild(passwordTd);
                  pwnedPasswordsTableBody.appendChild(tr);
              });
              pwnedPasswordsTable.style.display = 'block';
            });
          } else {
            noPwnedPasswordsSpan.style.display = 'block';
          }
        })
        .catch(error => {
          console.warn(error);
          alert(errorAlertMessage);
          passwordInput.disabled = false;
        });
    }
  });
});
const errorAlertMessage = `
An error occurred.


Here's some possible causes and solutions:

Problem: Incorrect password
Solution: Type the correct password

Problem: No internet access
Solution: Connect to the internet
`