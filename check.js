const Buttercup = require("buttercup");
const crypto = require('crypto');
const rp = require('request-promise');

const { Archive, Datasources, Credentials } = Buttercup;

const { FileDatasource } = Datasources;

function checkArchive(dataSource, password, returnTrash) {
  return new Promise(async (resolve, reject) => {
    const credentials = Credentials.fromPassword(password);
    const pwned = [];
    const requests = [];
    await dataSource
      .load(credentials)
      .then(Archive.createFromHistory)
      .then(archive => {
        data = archive.getGroups();
        data.forEach(group => {
          if (group) {
            if (group.isTrash() && !returnTrash) return;
            let entries = group._remoteObject.entries;
            if (entries) {
              entries.forEach(entry => {
                if (entry) {
                  if (entry.properties.title && entry.properties.username && entry.properties.password) {
                    requests.push(
                      isPwned(entry)
                    );
                  }
                }
              });
            }
          }
        });
      })
      .catch(function(err) {
        reject(err);
      });
    Promise.all(requests)
      .then(responses => {
        responses.forEach(response => {
          if (response) pwned.push(response);
        });
        resolve(pwned);
      })
      .catch(err => {
        console.warn('Failed:', err.message);
        reject(err);
      })
  });
}

function isPwned(entry, returnTrash) {
  return new Promise(async (resolve, reject) => {
    try {
      if (await pwIsPwned(entry.properties.password)) {
        resolve(entry.id);
      } else {
        resolve(false);
      }
    } catch (err) {
      reject(err);
    }
  });
}

function pwIsPwned(password) {
  return new Promise((resolve, reject) => {
    let shasum = crypto.createHash('sha1');
    shasum.update(password);
    let digest = shasum.digest('hex');
    rp.get(`https://api.pwnedpasswords.com/range/${digest.substring(0, 5)}`, (err, res, body) => {
      if (err) reject(err);
      try {
        let lines = body.split('\n');
        lines.forEach(line => {
          if (line.substring(0, 35).toUpperCase() == digest.substring(5).toUpperCase()) {
            resolve(true);
          }
        });
        resolve(false);
      } catch (err) {
        reject(err);
      }
    });
  });
}

module.exports = checkArchive;
